** Creating a new GIT **
git init
git clone https://ValleyMetro@bitbucket.org/ValleyMetro/ex-docs-tutorial.git

** Modify the execution of a file, if needed ** 
git update-index --chmod=+x deploy.sh

** Modify the globals, specifically user.name and user.email ** 
git config --global --edit
git config --global user.name "ValleyMetro"
git config --global user.email "buintsvc@valleymetro.org"

git status
git add --all

git commit -m 'Initial commit of deploy.sh'
git push
